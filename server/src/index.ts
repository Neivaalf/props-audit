import express from "express"
import cors from "cors"
import { ApolloServer, gql } from "apollo-server-express"
import { Sequelize } from "sequelize"
;(async () => {
  const typeDefs = gql`
    type Query {
      components(name: String): [Component]
    }

    type Component {
      name: String
      count: Int
      props(name: String): [Prop]
      places: [Place]
    }

    type Prop {
      name: String
      count: Int
      uniqueValues: Int
      values: [Value]
      valuesStats: [ValueStat]
    }

    type Value {
      filepath: String
      row: Int
      col: Int
      value: String
    }

    type ValueStat {
      value: String
      count: Int
    }

    type Place {
      filepath: String
      row: Int
      col: Int
    }
  `

  const resolvers = {
    Query: {
      components: async (_parent, args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT tag_name AS name, COUNT(*) AS count
          FROM (
            SELECT DISTINCT tag_name, filepath, tag_row, tag_col
            FROM audit
            ${args.name ? `WHERE tag_name="${args.name}"` : ""}
          )
          GROUP BY tag_name
          ORDER BY tag_name
        `)

        return res
      },
    },
    Component: {
      props: async (parent, args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT prop_name AS name, COUNT(*) AS count, tag_name
          FROM (
            SELECT *
            FROM audit
            WHERE tag_name="${parent.name}"
            ${args.name ? `AND prop_name="${args.name}"` : ""}
          )
          GROUP BY prop_name
        `)

        return res
      },
      places: async (parent, _args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT DISTINCT filepath, tag_row AS row, tag_col AS col
          FROM audit
          WHERE tag_name="${parent.name}"
        `)

        return res
      },
    },
    Prop: {
      values: async (parent, _args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT filepath, prop_row AS row, prop_col AS col, value
          FROM audit
          WHERE tag_name="${parent.tag_name}"
          AND prop_name="${parent.name}"
        `)

        return res
      },

      valuesStats: async (parent, _args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT value, COUNT(*) as count
            FROM (
              SELECT *
              FROM audit
              WHERE tag_name="${parent.tag_name}"
              AND prop_name="${parent.name}"
            )
          GROUP BY value
          ORDER BY count DESC
        `)

        return res
      },

      uniqueValues: async (parent, _args, ctx) => {
        const { sql }: { sql: Sequelize } = ctx

        const [res] = await sql.query(`
          SELECT COUNT(*) as count
            FROM (
              SELECT DISTINCT value
              FROM audit
              WHERE tag_name="${parent.tag_name}"
              AND prop_name="${parent.name}"
            )
          ORDER BY count DESC
        `)

        return res[0].count
      },
    },
  }

  const sql = new Sequelize({
    dialect: "sqlite",
    storage: "../database.db",
    logging: false,
  })

  try {
    await sql.authenticate()
    console.log("Connection has been established successfully.")
  } catch (error) {
    console.error("Unable to connect to the database:", error)
  }

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async () => {
      return {
        sql,
      }
    },
  })

  const app = express()
  app.use(cors())
  server.applyMiddleware({ app })

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  )
})()

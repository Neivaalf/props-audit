import React from "react"
import debounce from "lodash.debounce"

export default ({
  setGitHubOrigin,
}: {
  setGitHubOrigin: React.Dispatch<React.SetStateAction<string>>
}) => {
  const debouncedOnChangeGitHub = debounce(e => {
    setGitHubOrigin(e.target.value)
  }, 250)

  return (
    <div
      className={`fixed top-0 right-0 mt-64 z-20
    bg-white rounded-l shadow-xl
      transition-all duration-150 ease-in-out
      flex items-center
      transform w-11/12 lg:w-6/12
      translate-almost-full hover:translate-x-0`}
    >
      <i className="uil uil-github-alt text-2xl text-primary-400 block p-2"></i>
      <div className="p-2 w-full">
        <label className="flex items-center text-black text-opacity-75">
          <input
            type="text"
            name="repositoryFolderOrigin"
            id="repositoryFolderOrigin"
            className="bg-primary-100 text-black text-opacity-75 w-full p-2 rounded outline-none placeholder-primary-400 placeholder-opacity-75"
            placeholder="myName / myOrganization"
            onChange={e => {
              e.persist()
              debouncedOnChangeGitHub(e)
            }}
          />
        </label>
      </div>
    </div>
  )
}

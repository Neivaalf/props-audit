import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"
import { useParams, Link } from "react-router-dom"
import { animated } from "react-spring"

import { useDefaultEntranceTransitions } from "../utils"

import { FolderContext } from "../App"

import Loading from "./Loading"
import PrimaryCard from "./PrimaryCard"
import TopCard from "./TopCard"
import SearchInput from "./SearchInput"
import NoResults from "./NoResults"

type T_Prop = {
  name: string | null
  count: number
}

type T_Place = {
  filepath: string
  row: number
  col: number
}

type T_Component = {
  name: string
  count: number
  props: T_Prop[]
  places: T_Place[]
}

type T_Data = {
  components: T_Component[]
}

export default () => {
  const { component } = useParams<{ component: string }>()

  const COMPONENT_DETAILS = gql`
  {
      components(name: "${component}") {
        name
        count
        props {
            name
            count
        }
        places {
            filepath
            row
            col
        }
    }
  }
  `

  const { loading, error, data } = useQuery<T_Data, unknown>(COMPONENT_DETAILS)
  const [search, setSearch] = React.useState("")
  const [filteredProps, setFilteredProps] = React.useState<T_Prop[]>([])
  const [fileSearch, setFileSearch] = React.useState("")
  const [filteredFilepath, setFilteredFilepath] = React.useState<T_Place[]>([])

  const {
    repositoryFolderOrigin,
    repositoryFolderClient,
    gitHubOrigin,
  } = React.useContext(FolderContext)

  React.useEffect(() => {
    const _filteredProps = data?.components?.[0].props.filter(c => {
      if (!search) return true
      if (c.name?.toLowerCase().includes(search.toLowerCase())) return true

      return false
    })
    setFilteredProps(_filteredProps ?? [])
  }, [search, data])

  React.useEffect(() => {
    const _filteredFilepath = data?.components?.[0]?.places.filter(p => {
      if (!fileSearch) return true
      if (p.filepath?.toLowerCase().includes(fileSearch.toLowerCase()))
        return true

      return false
    })
    setFilteredFilepath(_filteredFilepath ?? [])
  }, [fileSearch, data])

  const transitions = useDefaultEntranceTransitions(6)

  if (loading) return <Loading />
  if (error || !data) return <p>Error :(</p>

  return (
    <div className="max-w-screen-xl mx-auto">
      <h1 className="text-primary-600 text-3xl mb-4 font-bold">
        <Link
          to="/"
          className="text-primary-200 hover:text-primary-400 transition-all duration-150 ease-in-out outline-none focus:text-primary-400"
        >
          <i className="uil uil-angle-left-b"></i>
        </Link>
        {component} ({data?.components[0]?.count})
      </h1>

      <div className="grid grid-cols-6 gap-4 mt-4">
        <PrimaryCard
          title="Number of props"
          value={data?.components[0].props.filter(p => !!p.name).length}
          transition={transitions[0]}
        />

        <TopCard title="Top 5 props" transition={transitions[1]}>
          {[...data.components?.[0].props]
            .sort((a, b) => b.count - a.count)
            .slice(0, 5)
            .map(c => {
              return (
                <div
                  className="block text-base text-black text-opacity-75"
                  key={c.name ?? "without props"}
                >
                  {c.name ?? "without props"} ({c.count})
                </div>
              )
            })}
        </TopCard>

        <h1 className="m-4 mb-2 ml-2 col-span-6 text-primary-400 font-bold">
          <i className="uil uil-brackets-curly"></i> List of Props (
          {filteredProps.length})
        </h1>
        <animated.div
          key={transitions[2].key}
          style={transitions[2].props}
          className="col-span-6 z-10 relative flex items-center"
        >
          <SearchInput
            placeholder="Filter on props (ie: color)"
            onChange={e => {
              setSearch(e.target.value)
            }}
          />
        </animated.div>
        <animated.div
          key={transitions[3].key}
          style={transitions[3].props}
          className="col-span-6 bg-white rounded-lg flex flex-col shadow-xl overflow-x-auto relative"
        >
          {filteredProps.length === 0 && <NoResults />}
          {filteredProps.length > 0 && (
            <table className="m-4 border-collapse table-auto flex-grow max-h-screen ">
              <thead className="text-primary-400 font-bold">
                <tr>
                  <th className="p-2 text-left">Prop</th>
                  <th className="p-2 text-left">Count</th>
                </tr>
              </thead>
              <tbody>
                {filteredProps.map((c, i) => (
                  <tr
                    key={c.name ?? "without props"}
                    className={`transition-all duration-150 ease-in-out ${
                      i % 2 === 1 ? "bg-primary-100" : ""
                    } ${
                      c.name === null
                        ? "italic opacity-25"
                        : "hover:bg-primary-200"
                    }`}
                  >
                    {!c.name && (
                      <>
                        <td className={"p-2 text-black text-opacity-75"}>
                          {c.name ?? "without props"}
                        </td>
                        <td className={"p-2 text-black text-opacity-75"}>
                          {c.count}
                        </td>
                      </>
                    )}
                    {c.name && (
                      <>
                        <td>
                          <Link
                            to={`/component/${component}/prop/${c.name}`}
                            className={
                              "block p-2 text-black text-opacity-75  outline-none focus:shadow-outline"
                            }
                          >
                            {c.name ?? "without props"}
                          </Link>
                        </td>
                        <td>
                          <Link
                            to={`/component/${component}/prop/${c.name}`}
                            className={"block p-2 text-black text-opacity-75"}
                            tabIndex={-1}
                          >
                            {c.count}
                          </Link>
                        </td>
                      </>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </animated.div>

        <h1 className="m-4 mb-2 ml-2 col-span-6 text-primary-400 font-bold">
          <i className="uil uil-file-edit-alt"></i> List of Places (
          {filteredFilepath.length})
        </h1>
        <animated.div
          key={transitions[4].key}
          style={transitions[4].props}
          className="col-span-6 z-10 relative flex items-center"
        >
          <SearchInput
            placeholder="Filter on filepath (ie: my-project)"
            onChange={e => {
              setFileSearch(e.target.value)
            }}
          />
        </animated.div>
        <animated.div
          key={transitions[5].key}
          style={transitions[5].props}
          className="col-span-6 bg-white rounded-lg flex flex-col shadow-xl overflow-x-auto relative"
        >
          {filteredFilepath.length === 0 && <NoResults />}
          {filteredFilepath.length > 0 && (
            <table className="m-4 border-collapse table-auto flex-grow max-h-screen ">
              <thead className="text-primary-400 font-bold">
                <tr>
                  <th className="p-2 text-left">Place</th>
                  <th className="p2 text-right">Github</th>
                </tr>
              </thead>
              <tbody>
                {filteredFilepath.map((p, i) => {
                  const replacedFilePath = p.filepath.replace(
                    repositoryFolderOrigin,
                    repositoryFolderClient
                  )

                  const [repository, ...path] = p.filepath
                    .replace(repositoryFolderOrigin, "")
                    .split("/")
                    .filter(Boolean)

                  return (
                    <tr
                      key={`${p.filepath}:${p.row}:${p.col}`}
                      className={`transition-all duration-150 ease-in-out hover:bg-primary-200 ${
                        i % 2 === 1 ? "bg-primary-100" : ""
                      }`}
                    >
                      <td>
                        <a
                          href={`vscode://file/${replacedFilePath}:${p.row}:${
                            p.col + 1
                          }`}
                          className="block w-full p-2 text-black text-opacity-75 outline-none focus:shadow-outline"
                        >
                          {replacedFilePath}:{p.row}:{p.col}
                        </a>
                      </td>
                      <td className="text-right">
                        <a
                          href={`https://github.com/${gitHubOrigin}/${
                            repository || ""
                          }/blob/master/${path.join("/") || ""}#L${p.row}`}
                          target="_blank"
                          className="p-2"
                        >
                          <i className="uil uil-github-alt text-primary-400 text-xl"></i>
                        </a>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
        </animated.div>
      </div>
    </div>
  )
}

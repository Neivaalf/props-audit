import React from "react"

export default ({
  placeholder,
  ...rest
}: React.InputHTMLAttributes<HTMLInputElement>) => (
  <>
    <input
      placeholder={placeholder}
      type="text"
      className="w-full p-5 
          rounded-lg outline-none
          bg-white 
          text-black text-opacity-75
          placeholder-primary-400 placeholder-opacity-75
          shadow-lg opacity-75
          focus:shadow-xl focus:opacity-100
          transition-all duration-150 ease-in-out"
      {...rest}
    />
    <i className="uil uil-search-alt absolute right-0 text-primary-200 text-2xl mr-4"></i>
  </>
)

import React from "react"
import { animated } from "react-spring"
import type { UseTransitionResult } from "react-spring"

export default ({
  transition,
  title,
  children,
}: {
  transition: UseTransitionResult<number, any>
  title: string
  children: React.ReactNode
}) => {
  return (
    <animated.div
      key={transition.key}
      style={transition.props}
      className="col-span-6 md:col-span-4 bg-white rounded-lg shadow-xl relative overflow-hidden z-0"
    >
      <i className="uil uil-medal absolute text-primary-100 iconBg z-10"></i>
      <h1 className="m-4 text-primary-400 font-bold relative z-20">{title}</h1>
      <div className="font-bold text-4xl m-5 relative z-20">{children}</div>
    </animated.div>
  )
}

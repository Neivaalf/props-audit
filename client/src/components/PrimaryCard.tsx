import React from "react"
import { animated, useSpring } from "react-spring"
import type { UseTransitionResult } from "react-spring"

export default ({
  transition,
  value,
  title,
}: {
  transition: UseTransitionResult<number, any>
  value: number
  title: string
}) => {
  const { total } = useSpring({
    total: value,
    from: { total: 0 },
  })

  return (
    <animated.div
      key={transition.key}
      style={transition.props}
      className="col-span-6 md:col-span-2 bg-primary-400 rounded-lg flex flex-col shadow-xl relative"
    >
      <h1 className="m-4 text-white font-bold">{title}</h1>
      <div className="flex-grow flex justify-center items-center text-white font-bold text-5xl m-10 mt-5">
        <animated.span>{total.interpolate(t => t.toFixed(0))}</animated.span>
      </div>
    </animated.div>
  )
}

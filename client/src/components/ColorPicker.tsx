import React from "react"

export default ({
  setTheme,
}: {
  setTheme: React.Dispatch<React.SetStateAction<string>>
}) => {
  return (
    <div className="fixed z-20 top-0 right-0 mt-10 bg-white rounded-l shadow-xl transition-all duration-150 ease-in-out flex items-center transform translate-almost-full hover:translate-x-0">
      <i className="uil uil-palette text-2xl text-primary-400 block p-2"></i>

      {["pink", "blue", "teal", "purple"].map(color => (
        <div
          key={color}
          className="p-2"
          onClick={() => {
            setTheme(color)
          }}
        >
          <div
            className={`rounded-full w-8 h-8
                bg-${color}-200 hover:bg-${color}-400
                transition-all duration-150 ease-in-out
                cursor-pointer`}
          ></div>
        </div>
      ))}
    </div>
  )
}

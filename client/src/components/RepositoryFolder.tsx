import React from "react"
import debounce from "lodash.debounce"

export default ({
  setRepositoryFolderOrigin,
  setRepositoryFolderClient,
}: {
  setRepositoryFolderOrigin: React.Dispatch<React.SetStateAction<string>>
  setRepositoryFolderClient: React.Dispatch<React.SetStateAction<string>>
}) => {
  const debouncedOnChangeOrigin = debounce(e => {
    setRepositoryFolderOrigin(e.target.value)
  }, 250)
  const debouncedOnChangeClient = debounce(e => {
    setRepositoryFolderClient(e.target.value)
  }, 250)

  return (
    <div
      className={`fixed top-0 right-0 mt-32 z-20
    bg-white rounded-l shadow-xl
      transition-all duration-150 ease-in-out
      flex items-center
      transform w-11/12 lg:w-6/12
      translate-almost-full hover:translate-x-0`}
    >
      <i className="uil uil-folder text-2xl text-primary-400 block p-2"></i>
      <div className="p-2 w-full">
        <label className="flex items-center mb-2 text-black text-opacity-75">
          <div className="w-20 text-right pr-2">Replace</div>
          <input
            type="text"
            name="repositoryFolderOrigin"
            id="repositoryFolderOrigin"
            className="bg-primary-100 text-black text-opacity-75 w-full p-2 rounded outline-none placeholder-primary-400 placeholder-opacity-75"
            placeholder="/home/user_name/"
            onChange={e => {
              e.persist()
              debouncedOnChangeOrigin(e)
            }}
          />
        </label>

        <label className="flex items-center mb-2 text-black text-opacity-75">
          <div className="w-20 text-right pr-2">By</div>
          <input
            type="text"
            name="repositoryFolderClient"
            id="repositoryFolderClient"
            className="bg-primary-100 text-black text-opacity-75 w-full p-2 rounded outline-none placeholder-primary-400 placeholder-opacity-75"
            placeholder="/Users/username/pathToYour/RepoFolder/"
            onChange={e => {
              e.persist()
              debouncedOnChangeClient(e)
            }}
          />
        </label>
      </div>
    </div>
  )
}

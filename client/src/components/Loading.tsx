import React from "react"

export default () => (
  <div className="h-screen w-screen overflow-auto bg-primary-100 text-primary-400 text-6xl p-4 flex items-center justify-center">
    <i className="uil uil-sync rotate-360"></i>
    <div className="font-bold">Loading...</div>
  </div>
)

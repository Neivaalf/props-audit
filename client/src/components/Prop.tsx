import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"
import { useParams, Link } from "react-router-dom"
import { animated } from "react-spring"

import { useDefaultEntranceTransitions } from "../utils"

import { FolderContext } from "../App"

import Loading from "./Loading"
import PrimaryCard from "./PrimaryCard"
import TopCard from "./TopCard"
import SearchInput from "./SearchInput"
import NoResults from "./NoResults"

type T_Value = {
  filepath: string
  row: number
  col: number
  value: string
}

type T_ValueStat = {
  value: string
  count: number
}

type T_Prop = {
  name: string | null
  count: number
  uniqueValues: number
  values: T_Value[]
  valuesStats: T_ValueStat[]
}

type T_Component = {
  name: string
  count: number
  props: T_Prop[]
}

type T_Data = {
  components: T_Component[]
}

export default () => {
  const { component, prop } = useParams<{ component: string; prop: string }>()
  const COMPONENT_DETAILS = gql`
  {
      components(name: "${component}") {
        name
        count
        props(name: "${prop}") {
            name
            count
            values {
                value
                filepath
                row
                col
            }
            uniqueValues
            valuesStats {
              value
              count
            }
        }
    }
  }
  `
  const [fileSearch, setFileSearch] = React.useState("")
  const [valueSearch, setValueSearch] = React.useState("")
  const [filteredValues, setFilteredValues] = React.useState<T_Value[]>([])

  const {
    repositoryFolderOrigin,
    repositoryFolderClient,
    gitHubOrigin,
  } = React.useContext(FolderContext)

  const { loading, error, data } = useQuery<T_Data, unknown>(COMPONENT_DETAILS)

  React.useEffect(() => {
    const _filteredValues = data?.components?.[0].props?.[0].values.filter(
      v => {
        let match = true

        if (
          fileSearch &&
          !v.filepath?.toLowerCase().includes(fileSearch.toLowerCase())
        )
          match = false

        if (
          valueSearch &&
          !v.value?.toLowerCase().includes(valueSearch.toLowerCase())
        )
          match = false

        return match
      }
    )
    setFilteredValues(_filteredValues ?? [])
  }, [fileSearch, valueSearch, data])

  const transitions = useDefaultEntranceTransitions(5)

  if (loading) return <Loading />
  if (error || !data) return <p>Error :(</p>

  return (
    <div className="max-w-screen-xl mx-auto">
      <h1 className="text-primary-600 text-3xl mb-4 font-bold">
        <Link
          to={`/`}
          className="text-primary-200 hover:text-primary-400 transition-all duration-150 ease-in-out outline-none focus:text-primary-400"
        >
          <i className="uil uil-angle-double-left"></i>
        </Link>
        <Link
          to={`/component/${component}`}
          className="text-primary-200 hover:text-primary-400 transition-all duration-150 ease-in-out mr-4 outline-none focus:text-primary-400"
        >
          {component} ({data.components?.[0].count})
        </Link>
        / {prop} ({data.components?.[0].props?.[0].count})
      </h1>

      <div className="grid grid-cols-6 gap-4 mt-4">
        <PrimaryCard
          title="Number of different values"
          value={data.components?.[0].props?.[0].uniqueValues}
          transition={transitions[0]}
        />

        <TopCard title="Top 5 values" transition={transitions[1]}>
          {data.components?.[0].props?.[0].valuesStats.slice(0, 5).map(v => {
            return (
              <div
                className="block text-base text-black text-opacity-75"
                key={v.value}
              >
                {v.value} ({v.count})
              </div>
            )
          })}
        </TopCard>

        <h1 className="m-4 mb-2 ml-2 col-span-6 text-primary-400 font-bold">
          <i className="uil uil-file-edit-alt"></i> List of Values (
          {filteredValues.length})
        </h1>
        <animated.div
          key={transitions[2].key}
          style={transitions[2].props}
          className="col-span-3 z-10 relative flex items-center"
        >
          <SearchInput
            placeholder="Filter on value (ie: primary)"
            onChange={e => {
              setValueSearch(e.target.value)
            }}
          />
        </animated.div>
        <animated.div
          key={transitions[3].key}
          style={transitions[3].props}
          className="col-span-3 z-10 relative flex items-center"
        >
          <SearchInput
            placeholder="Filter on filepath (ie: my-project)"
            onChange={e => {
              setFileSearch(e.target.value)
            }}
          />
        </animated.div>
        <animated.div
          key={transitions[4].key}
          style={transitions[4].props}
          className="col-span-6 bg-white rounded-lg flex flex-col shadow-xl overflow-x-auto relative"
        >
          {filteredValues.length === 0 && <NoResults />}
          {filteredValues.length > 0 && (
            <table className="m-4 border-collapse table-auto flex-grow">
              <thead className="text-primary-400 font-bold">
                <tr>
                  <th className="p-2 text-left">Values</th>
                  <th className="p-2 text-left">Filepath</th>
                  <th className="p2 text-right">Github</th>
                </tr>
              </thead>
              <tbody>
                {filteredValues.map((p, i) => {
                  const replacedFilePath = p.filepath.replace(
                    repositoryFolderOrigin,
                    repositoryFolderClient
                  )

                  const [repository, ...path] = p.filepath
                    .replace(repositoryFolderOrigin, "")
                    .split("/")
                    .filter(Boolean)

                  return (
                    <tr
                      key={`${p.filepath}:${p.row}:${p.col}`}
                      className={`transition-all duration-150 ease-in-out hover:bg-primary-200 ${
                        i % 2 === 1 ? "bg-primary-100" : ""
                      }`}
                    >
                      <td>
                        <a
                          href={`vscode://file/${replacedFilePath}:${p.row}:${
                            p.col + 1
                          }`}
                          className="block w-full p-2 text-black text-opacity-75 outline-none focus:shadow-outline"
                          title={`vscode://file/${replacedFilePath}:${p.row}:${
                            p.col + 1
                          }`}
                        >
                          {p.value}
                        </a>
                      </td>
                      <td>
                        <a
                          href={`vscode://file/${replacedFilePath}:${p.row}:${
                            p.col + 1
                          }`}
                          className="block p-2 text-black text-opacity-75 outline-none focus:shadow-outline"
                          title={`vscode://file/${replacedFilePath}:${p.row}:${
                            p.col + 1
                          }`}
                          tabIndex={-1}
                        >
                          {replacedFilePath}:{p.row}:{p.col}
                        </a>
                      </td>
                      <td className="text-right">
                        <a
                          href={`https://github.com/${gitHubOrigin}/${
                            repository || ""
                          }/blob/master/${path.join("/") || ""}#L${p.row}`}
                          target="_blank"
                          className="p-2"
                        >
                          <i className="uil uil-github-alt text-primary-400 text-xl"></i>
                        </a>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
        </animated.div>
      </div>
    </div>
  )
}

import React from "react"

export default () => (
  <div className="col-span-6 p-4 bg-white text-primary-200 text-5xl font-bold rounded-lg flex items-center justify-center h-64 shadow-xl overflow-x-auto">
    <i className="uil uil-search-alt"></i> No match
  </div>
)

import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"
import { Link } from "react-router-dom"
import { animated } from "react-spring"

import { useDefaultEntranceTransitions } from "../utils"

import Loading from "./Loading"
import PrimaryCard from "./PrimaryCard"
import TopCard from "./TopCard"
import SearchInput from "./SearchInput"
import NoResults from "./NoResults"

type T_Component = {
  name: string
  count: number
  props: {
    name: string
  }[]
}

type T_Data = {
  components: T_Component[]
}

const COMPONENTS = gql`
  {
    components {
      name
      count
      props {
        name
      }
    }
  }
`

export default () => {
  const { loading, error, data } = useQuery<T_Data, unknown>(COMPONENTS)
  const [search, setSearch] = React.useState("")
  const [filteredComponents, setFilteredComponents] = React.useState<
    T_Component[]
  >([])

  React.useEffect(() => {
    const _filteredComponents = data?.components.filter(c => {
      if (!search) return true
      if (c.name.toLowerCase().includes(search.toLowerCase())) return true

      return false
    })
    setFilteredComponents(_filteredComponents ?? [])
  }, [search, data])

  const transitions = useDefaultEntranceTransitions(4)

  if (loading) return <Loading />
  if (error || !data) return <p>Error :(</p>

  return (
    <div className="max-w-screen-xl mx-auto">
      <h1 className="text-primary-600 text-3xl mb-4 font-bold">Props audit</h1>
      <div className="grid grid-cols-6 gap-4 mt-4">
        <PrimaryCard
          title="Number of components"
          value={data.components.length}
          transition={transitions[0]}
        />

        <TopCard title="Top 5 components" transition={transitions[1]}>
          {[...data.components]
            .sort((a, b) => b.count - a.count)
            .slice(0, 5)
            .map(c => {
              return (
                <div
                  className="block text-base text-black text-opacity-75"
                  key={c.name}
                >
                  <span className="text-primary-400 text-opacity-50">
                    {"< "}
                  </span>
                  {c.name}
                  <span className="text-primary-400 text-opacity-50">
                    {" />"}
                  </span>{" "}
                  ({c.count})
                </div>
              )
            })}
        </TopCard>

        <h1 className="m-4 mb-2 ml-2 col-span-6 text-primary-400 font-bold">
          <i className="uil uil-arrow"></i> List of components (
          {filteredComponents.length})
        </h1>
        <animated.div
          key={transitions[2].key}
          style={transitions[2].props}
          className="col-span-6 z-10 relative flex items-center"
        >
          <SearchInput
            placeholder="Filter on name (ie: Typography)"
            onChange={e => {
              setSearch(e.target.value)
            }}
          />
        </animated.div>
        <animated.div
          key={transitions[3].key}
          style={transitions[3].props}
          className="col-span-6 bg-white rounded-lg flex flex-col shadow-xl overflow-x-auto relative"
        >
          {filteredComponents.length === 0 && <NoResults />}
          {filteredComponents.length > 0 && (
            <table className="m-4 border-collapse table-auto flex-grow">
              <thead className="text-primary-400 font-bold">
                <tr>
                  <th className="p-2 text-left">Component</th>
                  <th className="p-2 text-left">Count</th>
                  <th className="p-2 text-left">Props</th>
                </tr>
              </thead>
              <tbody>
                {filteredComponents.map((c, i) => (
                  <tr
                    key={c.name}
                    className={`transition-all duration-150 ease-in-out hover:bg-primary-200 ${
                      i % 2 === 1 ? "bg-primary-100" : ""
                    }`}
                  >
                    <td>
                      <Link
                        to={`/component/${c.name}`}
                        className="block p-2 text-black text-opacity-75 outline-none focus:shadow-outline"
                      >
                        {c.name}
                      </Link>
                    </td>
                    <td>
                      <Link
                        to={`/component/${c.name}`}
                        className="block p-2 text-black text-opacity-75"
                        tabIndex={-1}
                      >
                        {c.count}
                      </Link>
                    </td>
                    <td>
                      <Link
                        to={`/component/${c.name}`}
                        className="block p-2 text-black text-opacity-75"
                        tabIndex={-1}
                      >
                        {c.props.filter(p => !!p.name).length}
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </animated.div>
      </div>
    </div>
  )
}

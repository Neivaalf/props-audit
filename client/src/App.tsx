import React from "react"
import ApolloClient from "apollo-boost"
import { ApolloProvider } from "@apollo/react-hooks"
import { HashRouter as Router, Switch, Route } from "react-router-dom"

import ColorPicker from "./components/ColorPicker"
import RepositoryFolder from "./components/RepositoryFolder"
import GitHub from "./components/GitHub"
import LandingPage from "./components/LandingPage"
import Component from "./components/Component"
import Prop from "./components/Prop"

export const FolderContext = React.createContext({
  repositoryFolderOrigin: "",
  repositoryFolderClient: "",
  gitHubOrigin: "",
})

const App = () => {
  const [theme, setTheme] = React.useState("pink")
  const [repositoryFolderOrigin, setRepositoryFolderOrigin] = React.useState("")
  const [repositoryFolderClient, setRepositoryFolderClient] = React.useState("")
  const [gitHubOrigin, setGitHubOrigin] = React.useState("")

  const { protocol, hostname } = window.location

  const client = new ApolloClient({
    uri: `${protocol}//${hostname}:4000/graphql`,
  })

  return (
    <>
      <div className={`${theme}-theme min-h-screen bg-primary-100 p-4`}>
        <FolderContext.Provider
          value={{
            repositoryFolderOrigin,
            repositoryFolderClient,
            gitHubOrigin,
          }}
        >
          <ApolloProvider client={client}>
            <Router>
              <Switch>
                <Route exact path="/component/:component/prop/:prop">
                  <Prop />
                </Route>
                <Route exact path="/component/:component">
                  <Component />
                </Route>
                <Route path="/">
                  <LandingPage />
                </Route>
              </Switch>
            </Router>
          </ApolloProvider>
          <ColorPicker setTheme={setTheme} />
          <GitHub setGitHubOrigin={setGitHubOrigin} />
          <RepositoryFolder
            setRepositoryFolderOrigin={setRepositoryFolderOrigin}
            setRepositoryFolderClient={setRepositoryFolderClient}
          />
        </FolderContext.Provider>
      </div>
    </>
  )
}

export default App

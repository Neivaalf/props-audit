import { useTransition, config } from "react-spring"

export const useDefaultEntranceTransitions = (n: number) => {
  return useTransition(Array.from(Array(n).keys()), i => i, {
    from: { top: "2em", opacity: 0 },
    enter: { top: "0em", opacity: 1 },
    trail: 50,
    config: config.stiff,
  })
}

import fs from "fs"

import babelTraverse from "@babel/traverse"
import babelGenerator from "@babel/generator"
import sqlite3 from "sqlite3"

import { useEffect } from "react"

import { getAst, getTagName } from "../utils"

export default (program, filePaths, addLog) => {
  useEffect(() => {
    if (!filePaths.length) return

    sqlite3.verbose()

    try {
      const stats = fs.statSync(program.dbOutput)

      addLog({
        level: "debug",
        message: `File found: ${JSON.stringify(stats)}`,
      })

      try {
        fs.unlinkSync(program.dbOutput)

        addLog({
          level: "warning",
          message: JSON.stringify(`${program.dbOutput} deleted successfully`),
        })
      } catch (err) {
        addLog({
          level: "error",
          message: JSON.stringify(err),
        })
      }
    } catch (err) {
      addLog({
        level: "warning",
        message: JSON.stringify(err),
      })
    }

    const db = new sqlite3.Database(program.dbOutput)

    db.serialize(() => {
      db.run(`DROP TABLE IF EXISTS audit`)

      db.run(`CREATE TABLE IF NOT EXISTS audit (
        filepath TEXT NOT NULL,
        tag_name TEXT NOT NULL,
        tag_row INTEGER NOT NULL,
        tag_col INTEGER NOT NULL,
        prop_name TEXT,
        prop_row INTEGER,
        prop_col INTEGER,
        value TEXT
      );`)

      db.parallelize(() => {
        filePaths.forEach((filePath, index) => {
          addLog({
            message: `Parsing file ${index + 1}/${filePaths.length}`,
          })
          addLog({
            level: "debug",
            message: `${filePath}`,
          })

          try {
            addLog({
              level: "debug",
              message: "Transforming to AST",
            })
            const ast = getAst(filePath)

            addLog({
              level: "debug",
              message: "Looking for elements",
            })
            babelTraverse(ast, {
              JSXOpeningElement: path => {
                const { name, attributes } = path.node

                const tagName = getTagName(name)
                if (program.tags && !program.tags.includes(tagName)) {
                  return null
                }

                const { line: tagRow, column: tagCol } = path.node.loc.start

                addLog({
                  level: "debug",
                  message: `Found ${tagName} at L${tagRow}C${tagCol}`,
                })

                addLog({
                  level: "debug",
                  message: "Looking for props",
                })
                attributes.forEach(attribute => {
                  const propName = attribute?.name?.name

                  // Can't find the name of the prop
                  if (!propName) return null

                  // We take the code as it is, to skip parsing object etc...
                  const value = babelGenerator(attribute?.value).code

                  const { line: propRow, column: propCol } = attribute.loc.start
                  addLog({
                    level: "debug",
                    message: `Found ${propName} = ${value} at L${propRow}C${propCol}`,
                  })

                  db.run(
                    `INSERT INTO audit (
                    filepath,
                    tag_name,
                    tag_row,
                    tag_col,
                    prop_name,
                    prop_row,
                    prop_col,
                    value
                  ) VALUES (
                    ?, ?, ?, ?,
                    ?, ?, ?, ?
                  );`,
                    [
                      filePath,
                      tagName,
                      tagRow,
                      tagCol,
                      propName,
                      propRow,
                      propCol,
                      value,
                    ]
                  )
                })

                if (attributes.length === 0) {
                  // TODO: Register the tag even if it doesn't have props
                  db.run(
                    `INSERT INTO audit (
                    filepath,
                    tag_name,
                    tag_row,
                    tag_col,
                    prop_name,
                    prop_row,
                    prop_col,
                    value
                  ) VALUES (
                    ?, ?, ?, ?,
                    ?, ?, ?, ?
                  );`,
                    [filePath, tagName, tagRow, tagCol, null, null, null, null]
                  )
                }
              },
            })
          } catch (e) {
            console.error(e)
          }
        })
      })
    })
    addLog({
      message: "Writing to database",
    })

    // TODO: Report whitelisted components that were not scrapepd

    db.close(() => {
      addLog({
        message: `Audit complete : ${program.dbOutput}`,
      })
    })
  }, [filePaths])
}

import React from "react"
import { Color, Text } from "ink"

export default ({
  level,
  children,
}: {
  level?: "debug"
  children: React.ReactNode
}) => {
  if (level === "debug") {
    return (
      <Color blue>
        <Text bold>[DEBUG]</Text> {children}
      </Color>
    )
  }

  if (level === "error") {
    return (
      <Color red>
        <Text bold>[ERROR]</Text> {children}
      </Color>
    )
  }

  if (level === "warning") {
    return (
      <Color yellow>
        <Text bold>[WARNING]</Text> {children}
      </Color>
    )
  }

  return (
    <Text>
      <Text bold>[LOG]</Text> {children}
    </Text>
  )
}

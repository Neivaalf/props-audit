import React, { useReducer, useState, useEffect } from "react"
import { Box } from "ink"

import useFileFinder from "./useFileFinder"
import useParser from "./useParser"

import Log from "./Log"

export default ({ program, currentDirectory }) => {
  const [logs, addLog] = useReducer((state, newLog) => {
    if (program.debug || newLog.level !== "debug") {
      return [...state, newLog]
    }
    return state
  }, [])

  const [filePaths, setFilePaths] = useState([])

  useEffect(() => {
    addLog({
      level: "debug",
      message: `debug: ${JSON.stringify(program.debug)}`,
    })
    addLog({
      level: "debug",
      message: `tags: ${JSON.stringify(program.tags) || "all"}`,
    })
    addLog({
      level: "debug",
      message: `paths: ${JSON.stringify(program.paths)}`,
    })
    addLog({
      level: "debug",
      message: `dbOutput: ${JSON.stringify(program.dbOutput)}`,
    })
  }, [])

  useFileFinder(program, currentDirectory, addLog, setFilePaths)
  useParser(program, filePaths, addLog)

  return (
    <Box flexDirection="column">
      <Box flexDirection="column">
        {logs.map((log, i) => (
          <Log level={log.level} key={`${log.message} + ${i}`}>
            {log.message}
          </Log>
        ))}
      </Box>
    </Box>
  )
}

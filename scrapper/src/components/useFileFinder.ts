import path from "path"
import { useEffect } from "react"
import globby from "globby"

export default (program, currentDirectory, addLog, setFilePaths) => {
  useEffect(() => {
    ;(async () => {
      addLog({
        message: "Searching every jsx/tsx files",
      })
      const filePathsPromise: ReturnType<typeof globby>[] = program.paths.map(
        async (p: string) => {
          addLog({
            level: "debug",
            message: `Looking for files in: ${p}`,
          })

          return globby("**/**.{jsx,tsx}", {
            cwd:
              p !== currentDirectory
                ? path.resolve(currentDirectory, p)
                : currentDirectory,
            absolute: true,
            gitignore: true,
          })
        }
      )

      // @ts-ignore
      const filePaths: string[] = (await Promise.all(filePathsPromise)).flat()

      addLog({
        message: `${filePaths.length} file(s) found`,
      })

      filePaths.forEach(filePath => {
        addLog({
          level: "debug",
          message: filePath,
        })
      })

      setFilePaths(filePaths)
    })()
  }, [])
}

import { version } from "../../package.json"

import { program } from "commander"
import { render } from "ink"
import path from "path"
import React from "react"

const { clear } = console

import Main from "./components/Main"

clear()
;(async () => {
  program.version(version)
  const currentDirectory = process.cwd()

  program
    .option("-t, --tags <tags>", "comma separated list of tags", v =>
      v.split(",")
    )
    .option(
      "-p, --paths <paths>",
      "comma separated list of paths where we'll recursively look for tags",
      v => v.split(","),
      [currentDirectory]
    )
    .option("-d, --debug", "display trace", false)
    .option(
      "-dbo, --db-output <dbOutput>",
      "sqlite3 database filepath",
      path.resolve(currentDirectory, "./database.db")
    )

  program.parse()

  render(<Main program={program} currentDirectory={currentDirectory} />)
})()

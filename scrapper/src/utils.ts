import fs from "fs"

import { parse } from "@babel/parser"

export const getAst = filePath => {
  const code = fs.readFileSync(filePath, "utf-8")

  const ast = parse(code, {
    sourceType: "unambiguous",
    plugins: [
      filePath.endsWith(".tsx") ? "typescript" : "flow",
      "jsx",
      "classProperties",
      "decorators-legacy",
    ],
  })

  return ast
}

export const getTagName = name => {
  switch (name.type) {
    case "JSXMemberExpression": {
      return `${name.object.name}.${name.property.name}`
    }
    case "JSXIdentifier": {
      return name.name
    }
    default: {
      throw new Error(`Fail to fetch name from ${JSON.stringify(name)}`)
    }
  }
}
